var express = require('express')
var request = require('request')
var bodyParser = require('body-parser')

var app = express()


app.use(bodyParser.urlencoded({ extended: true }))
app.use(bodyParser.json())


var port = process.env.PORT || 1984

app.get('/', function(req, res) {
  res.send('success')
})


app.listen(port)
console.log('Up and running — what would you like to do?')
